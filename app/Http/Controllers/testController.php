<?php

namespace App\Http\Controllers;


use App\Http\Service\ViewEngine;

class testController extends Controller
{
    public function index()
    {

        $render = new ViewEngine();

        $Name = "Your name goes here";
        $Stuff = [
            [
                'Thing' => "roses",
                'Desc' => "red"
            ],
            [
                'Thing' => "violets",
                'Desc' => "blue"
            ],
            [
                'Thing' => "you",
                'Desc' => "able to solve this"
            ],
            [
                'Thing' => "we",
                'Desc' => "interested in you"
            ]
        ];
        return $render::render('extra', [
            'Name' => $Name, 'Stuff' => $Stuff
        ]);

    }
}
