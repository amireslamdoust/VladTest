<?php
/**
 * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
 * @since 7/15/18 - 3:02 AM
 */

namespace App\Http\Service;

class ViewEngine
{


    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @var array
     */
    protected static $arrayParameter = [];
    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @var array
     */
    protected static $stringParameter = [];
    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @var bool
     */
    protected static $error = false;

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param $viewPath
     * @return string
     */
    protected static function view($viewPath)
    {

        $path = resource_path('amir/' . $viewPath . '.tmpl');

        return $path;
    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param $viewPath
     * @param null $parameter
     */
    public static function render($viewPath, $parameter = null)
    {
        $path = self::view($viewPath);

        $data = self::loadTemplate($path, $parameter);

        echo $data;

    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param $template
     * @param $parameter
     */
    protected static function loadTemplate($template, $parameter)
    {

        $content = file_get_contents($template);

        return self::getParameter($content, $parameter);

    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param $content
     * @param $parameter
     * @return mixed
     */
    protected static function getParameter(&$content, $parameter)
    {

        self::detectParameter($parameter);

        if (count(self::$arrayParameter))
            self::eachVariable($content);


        if (count(self::$stringParameter))
            self::printVariable($content);

        return $content;

    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param $parameter
     */
    protected static function detectParameter($parameter)
    {

        if (count($parameter)) {

            foreach ($parameter as $key => $value) {

                if (is_string($value)) {
                    self::$stringParameter[$key] = $value;
                }

                if (is_array($value)) {
                    self::$arrayParameter[$key] = $value;
                }

            }

        }

    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param $content
     */
    protected static function printVariable(&$content)
    {

        $re = '/\{\{([A-Za-z0-9_]*)\}\}/m';

        preg_match_all($re, $content, $matches, PREG_SET_ORDER, 0);

        foreach ($matches as $value) {

            $content = str_replace($value[0], self::$stringParameter[$value[1]], $content);

        }

    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param $content
     */
    protected static function eachVariable(&$content)
    {

        $regexForEach = '/\{\{\#each ([A-Za-z_])([A-Za-z0-9_]*)\}\}/m';
        $tmpContent = [];

        preg_match_all($regexForEach, $content, $matches, PREG_OFFSET_CAPTURE, 0);

        if ($matches && count($matches) && isset($matches[0]) && is_array($matches[0]) && count($matches[0])) {

            $eachCounter = count($matches[0]);

            $regexForEachEnd = '/\{\{\/each\}\}/m';

            preg_match_all($regexForEachEnd, $content, $endMatches, PREG_OFFSET_CAPTURE, 0);

            if ($endMatches && count($endMatches) && isset($endMatches[0]) && is_array($endMatches[0]) && count($endMatches[0])) {

                if (count($endMatches[0]) == $eachCounter) {

                    foreach ($matches[0] as $key => $value) {

                        $res = self::showArrayContent($content, $value[1], $endMatches[0][$key][1]);
                        $tmpContent[$key]['main'] = $res['content'];
                        $tmpContent[$key]['cnt'] = $res['cnt'];

                    }

                    foreach ($tmpContent as $value) {

                        $content = str_replace($value['cnt'], $value['main'], $content);
                    }


                }

            }
        }

        self::$error = true;

    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param $content
     * @param $startPoint
     * @param $endPoint
     * @return array
     */
    private static function showArrayContent($content, $startPoint, $endPoint)
    {

        $cnt = substr($content, $startPoint, ($endPoint - $startPoint + 9));

        $regex = '/\{\{\#each ([A-Za-z0-9_]*)\}\}/m';

        $contentReplace = '';

        preg_match($regex, $cnt, $matches);

        if (isset($matches[1])) {

            $tempCnt = str_replace($matches[0], '', $cnt);
            $tempCnt = str_replace('{{/each}}', '', $tempCnt);

            $re = '/\{\{([^else]|[^\/unless][A-Za-z0-9_]*)\}\}/m';

            preg_match_all($re, $tempCnt, $inputs, PREG_SET_ORDER, 0);

            if (count($inputs) == 2) {

                $input1 = $inputs[0][0];
                $input2 = $inputs[1][0];

                $input1Val = $inputs[0][1];
                $input2Val = $inputs[1][1];

                $lastElement = end(self::$arrayParameter[$matches[1]]);

                foreach (self::$arrayParameter[$matches[1]] as $key => $value) {
                    $tmp = str_replace($input1, $value[$input1Val], $tempCnt);
                    $tmp = str_replace($input2, $value[$input2Val], $tmp);

                    $regexUnlessLast = '/\{\{\#(unless \@last)\}\}/m';

                    preg_match($regexUnlessLast, $tmp, $unlessMatch);

                    if (count($unlessMatch)) {

                        $regexMap = '/(?<=\{\{\#(unless \@last)\}\})(.*)(?=\{\{\/unless)/s';

                        preg_match($regexMap, $tmp, $matchLess);

                        if ($matchLess) {
                            $breakPoint = explode('{{else}}', $matchLess[0]);

                            if ($value == $lastElement) {
                                $tmp = str_replace('{{#unless @last}}' . $matchLess[0] . '{{/unless}}', $breakPoint[1], $tmp);

                            } else {
                                $tmp = str_replace('{{#unless @last}}' . $matchLess[0] . '{{/unless}}', $breakPoint[0], $tmp);

                            }
                        }

                    }


                    $contentReplace .= $tmp;

                }

                return ['content' => $contentReplace, 'cnt' => $cnt];
            }


        }

        self::$error = true;

    }


}